import cn.stard.utils.ESClientUtil;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Test;

import java.util.HashMap;

public class TestEs {

    /**
     * @Title:
     * @Description: 普通es里的java添加
     * @Author: Mr.She
     * @Version: 1.0
     * @Date:
     * @Parameters:
     * @Return
     */
    @Test
    public void testaddes(){
        // 添加索引(建库)建表设置文档id,并且通过map的方式添加字段
        // 1.获取es连接对象
        TransportClient client = ESClientUtil.getClient();
        // 2.准备放入es中的数据
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", "1");
        map.put("name", "我在源码学习Java!");
        map.put("age", "8");
        map.put("sex", "1");
        // 3.使用链式编程，将数据存入到ES中
        IndexRequestBuilder builder = client.prepareIndex("pethome", "user", "1").setSource(map);
        // 打印看存入到文档为1的数据
        System.out.println(builder.get());
    }
    /**
     * @Title:
     * @Description: 普通es里的java查询
     * @Author: Mr.She
     * @Version: 1.0
     * @Date:
     * @Parameters:
     * @Return
     */
    @Test
    public void testget(){
        // 添加索引(建库)建表设置文档id,并且通过map的方式添加字段
        // 1.获取es连接对象
        TransportClient client = ESClientUtil.getClient();
        // 先获得文档对象再获取他的全部
        GetResponse response = client.prepareGet("pethome", "user", "1").get();
        // 获取文档所有数据打印输出
        System.out.println(response);
        // 获取文档资源数据打印输出
        System.out.println(response.getSource());
    }
    /**
     * @Title:
     * @Description: 普通es里的java修改
     * @Author: Mr.She
     * @Version: 1.0
     * @Date:
     * @Parameters:
     * @Return
     */
    @Test
    public void testUpt(){
        // 添加索引(建库)建表设置文档id,并且通过map的方式添加字段
        // 1.获取es连接对象
        TransportClient client = ESClientUtil.getClient();
        // 2.准备要修改的数据
        HashMap<String, Object> map = new HashMap<>();
        map.put("name","我不喜欢java了");
        UpdateResponse updateDoc = client.prepareUpdate("pethome", "user", "1").setDoc(map).get();
        System.out.println(updateDoc);
    }

/**
* @Title:
* @Description: 普通es里的java删除
* @Author: Mr.She
* @Version: 1.0
* @Date:
* @Parameters:
* @Return
*/

    @Test
    public void testdele(){
        // 添加索引(建库)建表设置文档id,并且通过map的方式添加字段
        // 1.获取es连接对象
        TransportClient client = ESClientUtil.getClient();
        // 2.准备要修改的数据
        DeleteResponse deleteResponse = client.prepareDelete("pethome", "user", "1").get();
        System.out.println(deleteResponse);
    }



    /**
     * @Description: 批量添加数据
     * @Author: 笔上人间
     * @Date: 2022/7/23 0:02
     * @return: void
     **/
    @Test
    public void testBatch() throws Exception {
        // 1.获取ES连接
        TransportClient client = ESClientUtil.getClient();
        // 2.创建批量操作对象
        BulkRequestBuilder bulk = client.prepareBulk();
        // 3.批量创建文档，放入批量操作对象中
        for (int i = 2; i < 100; i++) {
            IndexRequestBuilder builder = client.prepareIndex("pethome", "user", i + "").setSource(
                    XContentFactory.jsonBuilder()
                            .startObject()
                            .field("id", i - 1)
                            .field("name", "我在源码学习Java!" + i)
                            .field("age", i - 1)
                            .field("sex", i%2)
                            .endObject()
            );
            BulkResponse result = bulk.add(builder).get();
            System.out.println(result.status());
        }
        // 4.关闭连接
        client.close();
    }

    /**
     * @Description: 复杂查询
     * @Author: 笔上人间
     * @Date: 2022/7/23 0:04
     * @return: void
     **/
    @Test
    public void testComplex() throws Exception {
        // index库type类型(相当于sql的表) document文件id 三要素
        // 1.获取ES连接
        TransportClient client = ESClientUtil.getClient();
        // 查询条件：- 查询用户表，name包含：我在源码，age在1~12之间，每页大小2，从二页开始查，按照age倒序
        // 2.得到搜索对象
        SearchRequestBuilder pethome = client.prepareSearch("pethome");
        // 3.指定要搜索的类型
        pethome.setTypes("user");

         // 4.指定Query搜索对象，当参数是接口时我们可以传递：接口的工具类、接口实现、匿名内部类，此处传递接口工具类
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        // 4.1.指定DSL查询，name包含我在源码
        BoolQueryBuilder must = boolQueryBuilder.must(QueryBuilders.matchQuery("name", "我在源码"));
        // 4.2.指定DSL过滤，age在1~12之间
     boolQueryBuilder.filter(QueryBuilders.rangeQuery("age").gte(1).lte(12))
                //性别
                .filter(QueryBuilders.termQuery("sex", 1));
        // 4.3.将搜索条件放入到搜索对象中
            pethome.setQuery(boolQueryBuilder);
         // 5.设置分页起始位置
        pethome.setFrom(2);
         // 6.设置分页每页展示条数
        pethome.setSize(2);
         // 7.设置排序，age倒序
        pethome.addSort("age",SortOrder.DESC);
         // 8.得到结果进行打印
        SearchHits hits = pethome.get().getHits();
        System.out.println("命中结果：" + hits.getTotalHits());
        // 遍历获得到的结果集
        for (SearchHit document : hits) {
            System.out.println(document.getSourceAsMap());
        }
    }

}
