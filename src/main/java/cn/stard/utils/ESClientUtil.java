package cn.stard.utils;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @BelongsProject: elasticSearch
 * @BelongsPackage: cn.itsource.utils
 * @Author: 笔上人间
 * @CreateTime: 2022-07-22  23:17
 * @Description: Java连接ES工具类
 * @Version: 1.0
 */
public class ESClientUtil {

    public static TransportClient getClient(){
        TransportClient client = null;
        Settings settings = Settings.builder()
                .put("cluster.name", "elasticsearch").build();
        try {
            client = new PreBuiltTransportClient(settings)
                    .addTransportAddress(new TransportAddress(InetAddress.getByName("127.0.0.1"), 9300));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return client;
    }

}